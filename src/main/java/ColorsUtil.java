import java.awt.*;

public class ColorsUtil {
    private static final int BLACK = 0;

    private final int maxIterations;
    private final int[] colors;

    public ColorsUtil(int maxIterations) {
        this.maxIterations = maxIterations;
        this.colors = new int[this.maxIterations + 1];
        init();
    }

    public int getRGBColor(int iteration) {
        if (iteration < maxIterations)
            return colors[iteration];
        else{
            return BLACK;
        }
    }

    private void init() {
        int scale = (255 * 2) / maxIterations;
        for (int i = 0; i < maxIterations / 2; i++) {
            float hue = 0xFF << 24 | (255 - i*scale) << 16 | i*scale << 8;
            colors[i] = Color.HSBtoRGB(hue, 1, i/(i+8f));
        }

        for (int i = 0; i < (maxIterations /2); i++){
            float hue = 0xFF000000 | (255-i*scale) << 8 | i*scale;
            colors[i+ maxIterations /2] = colors[i] = Color.HSBtoRGB(hue, 1, i/(i+8f));
        }
    }

}
