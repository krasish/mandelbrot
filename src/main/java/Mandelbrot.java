import org.apache.commons.cli.*;

public class Mandelbrot {
    private int width;
    private int height;
    private int threads;
    private int granularity;
    private boolean outputImage;
    private boolean dynamicExecution;
    private String outputImagePath;

    public static void main(String... args) {
        Mandelbrot m = new Mandelbrot();
        try {
            m.parseCommands(args);
        } catch (ParseException e) {
            System.out.println("Could not parse arguments");
        }

        MandelbrotExecutor executor = new MandelbrotExecutor(m.width, m.height, m.threads, m.granularity, m.outputImage, m.outputImagePath);
        if(m.dynamicExecution){
            executor.executeDynamic();
        } else {
            executor.executeStatic();
        }
    }

    private void parseCommands(String... args) throws ParseException {
        CommandLineParser parser = new DefaultParser();

        Options options = new Options();
        options.addRequiredOption("s", "size", true, "image resolution in format: <WIDTH>x<HEIGHT>");
        options.addRequiredOption("t", "threads", true, "number of threads");
        options.addOption("d", "dynamic", false, "use dynamic task distribution");
        options.addOption("g", "granularity", true, "number of parts to divide the picture in");
        options.addOption("o", "output", true, "path in which a png image will be saved after completion");

        CommandLine parsed = parser.parse(options, args);

        String[] sizes = parsed.getOptionValue("s").split("x");
        this.width = Integer.parseInt(sizes[0]);
        this.height = Integer.parseInt(sizes[1]);

        String threadsString = parsed.getOptionValue("t");
        this.threads = Integer.parseInt(threadsString);

        if (parsed.hasOption('g')) {
            String granularityString = parsed.getOptionValue("g");
            this.granularity = Integer.parseInt(granularityString);
        } else {
            this.granularity = this.threads;
        }

        outputImage = parsed.hasOption("o");
        if (outputImage) {
            outputImagePath = parsed.getOptionValue("o");
        }

        dynamicExecution = parsed.hasOption("d");
    }
}
